-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Ven 01 Mai 2015 à 18:05
-- Version du serveur :  5.5.38
-- Version de PHP :  5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `casip`
--

-- --------------------------------------------------------

--
-- Structure de la table `dons`
--

CREATE TABLE `dons` (
`id_dons` int(11) NOT NULL,
  `id_membres` int(11) NOT NULL,
  `montant_membres` float(6,2) NOT NULL,
  `date_dons` datetime NOT NULL,
  `valide_dons` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `type_dons` smallint(6) NOT NULL,
  `mode_dons` smallint(6) NOT NULL,
  `nature_dons` int(11) NOT NULL,
  `pdf_dons` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `id_erreur` enum('0','1','2','3','4','5','6') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `dons`
--

INSERT INTO `dons` (`id_dons`, `id_membres`, `montant_membres`, `date_dons`, `valide_dons`, `type_dons`, `mode_dons`, `nature_dons`, `pdf_dons`, `id_erreur`) VALUES
(1, 1, 444.00, '2015-04-26 21:48:00', '1', 3, 1, 1, '000521L', '0'),
(2, 1, 55.00, '2015-04-26 21:48:00', '1', 3, 3, 1, '', '0'),
(3, 1, 444.00, '2015-04-26 21:48:00', '1', 2, 2, 1, '', '0'),
(6, 1, 444.00, '2015-04-26 21:48:00', '1', 0, 0, 0, '', '0'),
(7, 5, 26.00, '2015-04-19 17:14:00', '1', 0, 0, 0, '', '0'),
(8, 8, 0.00, '2015-04-19 17:24:00', '1', 0, 0, 0, '000552L', '0'),
(9, 5, 0.00, '2015-04-19 17:44:00', '1', 0, 0, 0, '', '0'),
(10, 5, 0.00, '2015-04-19 17:53:00', '1', 0, 0, 0, '', '0'),
(11, 5, 0.00, '2015-04-19 18:09:00', '1', 0, 0, 0, '', '0'),
(12, 10, 0.00, '2015-04-20 16:33:00', '1', 0, 0, 0, '', '0'),
(13, 1, 444.00, '2015-04-26 21:48:00', '1', 0, 0, 0, '', '0'),
(14, 1, 444.00, '2015-04-26 21:48:00', '1', 0, 0, 0, '', '0'),
(18, 1, 444.00, '2015-04-26 21:48:00', '0', 0, 0, 0, '', '0');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `dons`
--
ALTER TABLE `dons`
 ADD PRIMARY KEY (`id_dons`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `dons`
--
ALTER TABLE `dons`
MODIFY `id_dons` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
