-- phpMyAdmin SQL Dump
-- version 4.2.10
-- http://www.phpmyadmin.net
--
-- Client :  localhost:8889
-- Généré le :  Ven 01 Mai 2015 à 18:05
-- Version du serveur :  5.5.38
-- Version de PHP :  5.6.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `casip`
--

-- --------------------------------------------------------

--
-- Structure de la table `membres`
--

CREATE TABLE `membres` (
`id_membres` int(11) NOT NULL,
  `email_membres` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nom_membres` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `prenom_membres` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `passe_membres` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `date_membres` date NOT NULL,
  `actif_membres` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `adresse_membres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cp_membres` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `ville_membres` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `naissance_membres` date NOT NULL,
  `news_membres` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `tel_membres` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `port_membres` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `societe_membres` varchar(155) COLLATE utf8_unicode_ci NOT NULL,
  `pays_membres` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `nivo_membres` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `type_membres` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `civ_membres` smallint(6) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Contenu de la table `membres`
--

INSERT INTO `membres` (`id_membres`, `email_membres`, `nom_membres`, `prenom_membres`, `passe_membres`, `date_membres`, `actif_membres`, `adresse_membres`, `cp_membres`, `ville_membres`, `naissance_membres`, `news_membres`, `tel_membres`, `port_membres`, `societe_membres`, `pays_membres`, `nivo_membres`, `type_membres`, `civ_membres`) VALUES
(1, 'sebti@medya-c.com', 'Dupond', 'Jacques', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-06', '1', '134ruedecharenton', '75012', 'Paris', '0000-00-00', '1', '0897976867', '0000000000', '0', 'France', '1', '0', 1),
(2, 'noogra@gmail.com', 'kjgkjg', 'jgjkgjgkj', '316f5f1a806dc5a2244602806b8f49faab90635842acd2760da551615005686a213b78f4ebe99cb27231d051e54c2f6fd4e5a78fa8c6cdb3c8c9d6398b297b18', '2015-04-11', '1', 'gkjg', '', 'gjkgjk', '0000-00-00', '1', 'jgjkg', '0000000000', '0', 'kjgjkg', '1', '0', 1),
(5, 'sebtitoto@gmail.com', 'jacques', 'Lamotta', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-16', '1', 'ruedelouest', '97100', 'Armanville', '0000-00-00', '1', '87687854', '0000000000', '0', 'France', '1', '0', 1),
(8, 'toto@laposte.net', 'marc', 'champoing', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-19', '1', '33ruedelarbre', '75015', 'Paris', '0000-00-00', '1', '0132415217', '0000000000', '0', 'France', '1', '0', 1),
(9, 'sebtitoto@gmail.com', 'jacques', 'Soussou', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-19', '1', 'ruedelarbre', '75016', 'Paris', '0000-00-00', '1', '0142654321', '0000000000', '0', 'France', '1', '0', 1);

--
-- Index pour les tables exportées
--

--
-- Index pour la table `membres`
--
ALTER TABLE `membres`
 ADD PRIMARY KEY (`id_membres`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `membres`
--
ALTER TABLE `membres`
MODIFY `id_membres` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
