-- phpMyAdmin SQL Dump
-- version 4.0.10.6
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1:3306
-- Время создания: Май 02 2015 г., 22:15
-- Версия сервера: 5.5.41-log
-- Версия PHP: 5.3.29

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `casip`
--

-- --------------------------------------------------------

--
-- Структура таблицы `civilites`
--

CREATE TABLE IF NOT EXISTS `civilites` (
  `id_civ` smallint(6) NOT NULL,
  `titre_civ` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `ordre_civ` smallint(6) NOT NULL,
  PRIMARY KEY (`id_civ`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Дамп данных таблицы `civilites`
--

INSERT INTO `civilites` (`id_civ`, `titre_civ`, `ordre_civ`) VALUES
(1, 'Monsieur', 1),
(2, 'Madame', 2);

-- --------------------------------------------------------

--
-- Структура таблицы `dons`
--

CREATE TABLE IF NOT EXISTS `dons` (
  `id_dons` int(11) NOT NULL AUTO_INCREMENT,
  `id_membres` int(11) NOT NULL,
  `montant_membres` float(6,2) NOT NULL,
  `date_dons` datetime NOT NULL,
  `valide_dons` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `type_dons` smallint(6) NOT NULL,
  `mode_dons` smallint(6) NOT NULL,
  `nature_dons` int(11) NOT NULL,
  `pdf_dons` varchar(7) COLLATE utf8_unicode_ci NOT NULL,
  `id_erreur` enum('0','1','2','3','4','5','6') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  PRIMARY KEY (`id_dons`),
  KEY `id_membres` (`id_membres`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=26 ;

--
-- Дамп данных таблицы `dons`
--

INSERT INTO `dons` (`id_dons`, `id_membres`, `montant_membres`, `date_dons`, `valide_dons`, `type_dons`, `mode_dons`, `nature_dons`, `pdf_dons`, `id_erreur`) VALUES
(1, 1, 444.00, '2015-04-26 21:48:00', '1', 3, 1, 1, '000521L', '0'),
(2, 1, 55.00, '2015-04-26 21:48:00', '1', 3, 3, 1, '', '0'),
(3, 1, 444.00, '2015-04-26 21:48:00', '1', 2, 2, 1, '', '0'),
(6, 1, 444.00, '2015-04-26 21:48:00', '1', 0, 0, 0, '', '0'),
(7, 5, 26.00, '2015-04-19 17:14:00', '1', 0, 0, 0, '', '0'),
(8, 8, 0.00, '2015-04-19 17:24:00', '1', 0, 0, 0, '000552L', '0'),
(9, 5, 0.00, '2015-04-19 17:44:00', '1', 0, 0, 0, '', '0'),
(10, 5, 0.00, '2015-04-19 17:53:00', '1', 0, 0, 0, '', '0'),
(11, 5, 0.00, '2015-04-19 18:09:00', '1', 0, 0, 0, '', '0'),
(12, 9, 0.00, '2015-04-20 16:33:00', '1', 0, 0, 0, '', '0'),
(13, 1, 444.00, '2015-04-26 21:48:00', '1', 0, 0, 0, '', '0'),
(14, 1, 444.00, '2015-04-26 21:48:00', '1', 0, 0, 0, '', '0'),
(18, 1, 444.00, '2015-04-26 21:48:00', '0', 0, 0, 0, '', '0'),
(22, 1, 555.00, '2015-05-02 00:00:00', '1', 2, 2, 2, '', '0'),
(25, 1, 1000.00, '2015-05-02 00:00:00', '1', 3, 3, 3, '', '0');

-- --------------------------------------------------------

--
-- Структура таблицы `membres`
--

CREATE TABLE IF NOT EXISTS `membres` (
  `id_membres` int(11) NOT NULL AUTO_INCREMENT,
  `email_membres` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nom_membres` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `prenom_membres` varchar(75) COLLATE utf8_unicode_ci NOT NULL,
  `passe_membres` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `date_membres` date NOT NULL,
  `actif_membres` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `adresse_membres` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cp_membres` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `ville_membres` varchar(120) COLLATE utf8_unicode_ci NOT NULL,
  `naissance_membres` date NOT NULL,
  `news_membres` enum('0','1') COLLATE utf8_unicode_ci NOT NULL,
  `tel_membres` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `port_membres` varchar(13) COLLATE utf8_unicode_ci NOT NULL,
  `societe_membres` varchar(155) COLLATE utf8_unicode_ci NOT NULL,
  `pays_membres` varchar(150) COLLATE utf8_unicode_ci NOT NULL,
  `nivo_membres` enum('0','1','2') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `type_membres` enum('0','1') COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `civ_membres` smallint(6) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id_membres`),
  KEY `civ_membres` (`civ_membres`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `membres`
--

INSERT INTO `membres` (`id_membres`, `email_membres`, `nom_membres`, `prenom_membres`, `passe_membres`, `date_membres`, `actif_membres`, `adresse_membres`, `cp_membres`, `ville_membres`, `naissance_membres`, `news_membres`, `tel_membres`, `port_membres`, `societe_membres`, `pays_membres`, `nivo_membres`, `type_membres`, `civ_membres`) VALUES
(1, 'sebti@medya-c.com', 'Dupond', 'Jacques', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-06', '1', '134ruedecharenton', '75012', 'Paris', '0000-00-00', '1', '0897976867', '0000000000', '0', 'France', '1', '0', 1),
(2, 'noogra@gmail.com', 'kjgkjg', 'jgjkgjgkj', '316f5f1a806dc5a2244602806b8f49faab90635842acd2760da551615005686a213b78f4ebe99cb27231d051e54c2f6fd4e5a78fa8c6cdb3c8c9d6398b297b18', '2015-04-11', '1', 'gkjg', '', 'gjkgjk', '0000-00-00', '1', 'jgjkg', '0000000000', '0', 'kjgjkg', '1', '0', 1),
(5, 'sebtitoto@gmail.com', 'jacques', 'Lamotta', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-16', '1', 'ruedelouest', '97100', 'Armanville', '0000-00-00', '1', '87687854', '0000000000', '0', 'France', '1', '0', 1),
(8, 'toto@laposte.net', 'marc', 'champoing', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-19', '1', '33ruedelarbre', '75015', 'Paris', '0000-00-00', '1', '0132415217', '0000000000', '0', 'France', '1', '0', 1),
(9, 'sebtitoto@gmail.com', 'jacques', 'Soussou', '309cace65801ff1000b969524ebf72cc7680e5c44e08997004a99d55ae7f3f8d6cb3fcea6668be9e2adb01141b633892dcee9997d0f268b7c35b4c3b0135a165', '2015-04-19', '1', 'ruedelarbre', '75016', 'Paris', '0000-00-00', '1', '0142654321', '0000000000', '0', 'France', '1', '0', 1);

--
-- Ограничения внешнего ключа сохраненных таблиц
--

--
-- Ограничения внешнего ключа таблицы `dons`
--
ALTER TABLE `dons`
  ADD CONSTRAINT `dons_ibfk_1` FOREIGN KEY (`id_membres`) REFERENCES `membres` (`id_membres`);

--
-- Ограничения внешнего ключа таблицы `membres`
--
ALTER TABLE `membres`
  ADD CONSTRAINT `membres_ibfk_1` FOREIGN KEY (`civ_membres`) REFERENCES `civilites` (`id_civ`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
