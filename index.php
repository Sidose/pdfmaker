<?
session_start();
$message = isset($_SESSION['nomembres']) ? 'Aucune utilisateur' : '';
$classForMesage = $message ? 'message' : 'none';
unset($_SESSION['nomembres']);
?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Member Account</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="css/style.css" type="text/css" />
</head>
<body>
<div id="main" class="centre">
    <form id="fndAccount" class="shadowBox" name="account" action="pages/account.php" method="post">
        <div class="<?=$classForMesage?>"><?= $message ?></div>
        <p>
            <label><span>Nom:</span> <input type="text" name="nom" required /></label>
        </p>
        <p>
            <label><span>Nom de famille:</span> <input type="text" name="nomDeFam" required/></label>
        </p>
        <p>
            <input type="submit" name="account" value="montrer le compte" />
        </p>
    </form>

    <hr class="clear">

    <form id="allDons" class="shadowBox" name="pdf" action="pages/pdf.php" method="post">
        <p>
            Tous les dons:
        </p>
        <p>
            <input type="submit" name="show" value="montrer tous les dons" />
        </p>
    </form>
 </div>
</body>
</html>

