<?php
error_reporting(E_ALL);

class PDOp
{
    protected $PDO;
    protected $dsn = 'mysql:host=localhost;dbname=casip';
    protected $user = "mysql";
    protected $pass = "mysql";
    protected $opt = array(
        PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8',
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
        PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
    );

    public function __construct()
    {
        $this->PDO = new PDO($this->dsn, $this->user, $this->pass, $this->opt);
    }

    public function getAll()
    {
        $stmt = $this->PDO->prepare('SELECT d.date_dons,m.nom_membres,m.prenom_membres,d.id_dons,d.montant_membres,d.pdf_dons,d.valide_dons,d.id_erreur FROM dons AS d,membres AS m WHERE d.id_membres = m.id_membres');
        $stmt->execute();
        $data = array();

        $i = 0;
        foreach ($stmt as $row) {
            $data[$i++] = $row;
        }
        return $data;
    }

    public function checkIfExisted($nom_membres, $prenom_membres){
        $stmt = $this->PDO->prepare('SELECT m.id_membres FROM membres AS m WHERE m.nom_membres =:nom_membres AND m.prenom_membres =:prenom_membres');
        $stmt->execute(array('nom_membres' => $nom_membres, 'prenom_membres' => $prenom_membres));
        $data = array();
        $data = $stmt->fetch(PDO::FETCH_ASSOC);
        if($data) return true;
        return false;
    }

    public function getbyName($nom_membres, $prenom_membres)
    {
        $stmt = $this->PDO->prepare('SELECT d.date_dons,d.montant_membres,d.pdf_dons,d.valide_dons FROM dons AS d,membres AS m WHERE m.nom_membres =:nom_membres AND m.prenom_membres =:prenom_membres AND d.id_membres=m.id_membres');
        $stmt->execute(array('nom_membres' => $nom_membres, 'prenom_membres' => $prenom_membres));
        $data = array();

        $i = 0;
        foreach ($stmt as $row) {
            $data[$i++] = $row;
        }

        return $data;
    }

    public function getForTemplate($id_dons)
    {
        $stmt = $this->PDO->prepare('SELECT c.titre_civ,m.nom_membres,m.prenom_membres,m.adresse_membres,m.cp_membres,m.ville_membres,d.montant_membres,d.type_dons,d.nature_dons,d.mode_dons,d.pdf_dons,d.date_dons,d.valide_dons FROM dons AS d,membres AS m,civilites AS c WHERE d.id_dons =:id_dons AND d.id_membres = m.id_membres AND m.civ_membres=c.id_civ');
        $stmt->execute(array('id_dons' => $id_dons));
        $data = array();

        $i = 0;
        foreach ($stmt as $row) {
            $data[$i++] = $row;
        }

        return $data;
    }
}
