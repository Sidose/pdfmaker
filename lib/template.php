<?php

require_once('lib_pdf.php');
require_once('../lib/lib_msql.php');
define('EURO', chr(128)); //€

function makePdf(array $inParams)
{
    if (count($inParams) != 13) {
        return false;
    }
    $valideDons = isset($inParams['valide_dons']) ? $inParams['valide_dons'] : '1';

    if ($valideDons != 1) {
        return false;
    }

    $titreCiv = isset($inParams['titre_civ']) ? $inParams['titre_civ'] : 'Monsieur';
    $nomMembres = isset($inParams['nom_membres']) ? $inParams['nom_membres'] : 'Gouz';
    $prenomMembres = isset($inParams['prenom_membres']) ? $inParams['prenom_membres'] : 'Sylvain';
    $adresseMembres = isset($inParams['adresse_membres']) ? $inParams['adresse_membres'] : '16 rue des Martyrs';
    $cpMembres = isset($inParams['cp_membres']) ? $inParams['cp_membres'] : '75009';
    $villeMembres = isset($inParams['ville_membres']) ? $inParams['ville_membres'] : 'PARIS';
    $montantMembres = isset($inParams['montant_membres']) ? $inParams['montant_membres'] : '50.00';
    $typeDons = isset($inParams['type_dons']) ? $inParams['type_dons'] : '3';
    $natureDons = isset($inParams['nature_dons']) ? $inParams['nature_dons'] : '1';
    $modeDons = isset($inParams['mode_dons']) ? $inParams['mode_dons'] : '3';
    $pdfDons = isset($inParams['pdf_dons']) ? $inParams['pdf_dons'] : '000552L';
    $dateDons = isset($inParams['date_dons']) ? date("d/m/Y", strtotime($inParams['date_dons'])) : date("d/m/Y");
    $currYearLastTwoDigit = isset($inParams['date_dons']) ? date('y',strtotime($inParams['date_dons'])): date("y");

    $pdf = new FPDF('L', 'mm', 'A4');
    $pdf->AddPage();
    $pdf->SetFont('Arial', '', 9);
    $pdf->Ln(23);
//$pdf->Ln(5);

    $pdf->Image('../images/logo.png', 16, 12);

    $pdf->SetLeftMargin(16);
    $pdf->SetFontSize(6.3);
    $pdf->Cell(0, 3, utf8_decode("RECONNUE D'UTILITE PUBLIQUE"), 0, 1);
    $pdf->SetFontSize(7);
    $pdf->Cell(0, 3, utf8_decode("Par décret le 29 décembre 1999"), 0, 0);
    $pdf->SetFontSize(8.8);
    $pdf->SetX(115);
    $pdf->Cell(0, 0, utf8_decode("Paris, le {$dateDons}"), 0, 0);
    $pdf->Ln(4);
    $pdf->Cell(0, 4, utf8_decode($titreCiv . ","), 0, 1);
    $pdf->Cell(0, 14, utf8_decode("Merci de votre générosité et de votre don."), 0, 1);

    $strings = array(
        utf8_decode("Vous trouverez ci-contre un reçu détachable, à joindre à votre prochaine déclaration"),
        utf8_decode("fiscale. Il vous donne le droit :"),
        utf8_decode("- impôt sur le revenu : à une réduction d'impôt égale à 75% des dons jusqu'à"),
        utf8_decode("concurrence de 526") . EURO . utf8_decode(", annuellement. Au-delà de cette somme, la déduction est de"),
        utf8_decode("66%, dans la limite de 20% du revenu imposable."),
        utf8_decode("- impôt de solidarité sur la fortune (ISF) : à une réduction égale à 75% de vos dons"),
        utf8_decode("jusqu'à concurrence de 66 666 ") . EURO . utf8_decode("."),
        utf8_decode("Le même don ne peut évidemment être déduit qu'une fois."),
    );


//var_dump($strings);
    foreach ($strings as $index => $str) {
        $pdf->Cell(0, 4, $str, 0, 1);
    }
    $pdf->Ln(4);
    $pdf->Cell(0, 4, utf8_decode("(voir également notre site web : http://www.casip-cojasor.fr/donation/fisc.htm')")); // . $pdf->Write(5,"http://www.casip-cojasor.fr/donation/fisc.htm","http://www.casip-cojasor.fr/donation/fisc.htm") ,0,1);
    $pdf->Ln(8);
    $pdf->Cell(0, 4, utf8_decode("Au nom de notre conseil d'administration et des personnes que nous aidons, nous"), 0, 1);
    $pdf->Cell(0, 4, utf8_decode("vous adressons, " . $titreCiv . ", nos vifs remerciements et l'assurance de nos sentiments"), 0, 1);
    $pdf->Cell(0, 4, utf8_decode("distingués."), 0, 1);

    $pdf->SetX(90);
    $pdf->Cell(20, 4, utf8_decode("La Directrice :"), 0, 1);
    $pdf->Ln(25);

//////////////////////////////////////
    $pdf->Rect(17, 142, 128, 33);
    $pdf->SetFont('', 'B');
    $pdf->SetFontSize(10);
    $pdf->MultiCell(130, 4, utf8_decode("Au titre de l'impôt sur le revenu, votre droit à déduction fiscale est de
37,50") . EURO . utf8_decode("(1) dans la limite de 20% de votre revenu imposable.
L'excédent est reportable sur 5 ans.
Au titre de l'ISF, votre droit à déduction fiscale est de 37,50 ") . EURO . utf8_decode(" dans la
limite de 50 000 euros.
Ces déductions ne sont pas cumulables."), 0, 'C');
    $pdf->Ln(4);
    $pdf->SetFont('', 'I');
    $pdf->SetFontSize(8);
    $pdf->SetX(17);
    $pdf->Cell(0, 4, utf8_decode("(1) En fonction de vos dons, cumulés annuellement"), 0, 1);
//////////////////////////////////////

    $pdf->Ln(2);
    $pdf->SetFont('', '');
    $pdf->MultiCell(130, 3, utf8_decode("NB - Votre don, destiné à la fondation \"CASIP-COJASOR\", est intégralement affecté, sans 
retenue d'aucune sorte, aux aides financières directes attribuées aux familles aidées 
(alimentation, logement, etc...)"), 0, 'L');


    $pdf->SetLineWidth(0.4);
    $pdf->SetDrawColor(150, 150, 150);
    $pdf->Line(17, 190, 145, 190);
    $pdf->SetFontSize(7);
    $pdf->Text(17, 195, utf8_decode("8, Rue de Pali-Kao, 75020 PARIS - 01.44.62.13.13"));
    $pdf->SetDrawColor(0, 0, 0);

// vertical separate line
    $pdf->Line(147, 12, 147, 198);

// right half of page
    $pdf->Image('../images/logo.png', 297 / 2 + 2, 12);
    $pdf->SetFontSize(9);
    $pdf->SetY(30);
    $pdf->SetX(297 / 2 + 2);

    $pdf->SetDrawColor(150, 150, 150);
    $pdf->SetFont('', 'B');
    $pdf->SetFontSize(12);

    $pdf->SetY(13);
    $pdf->SetX(228);
    $rightTitleWidth = $pdf->GetStringWidth(utf8_decode("RECU N° {$currYearLastTwoDigit}/{$pdfDons}")) + 10;
    $pdf->Cell($rightTitleWidth, 7, utf8_decode("RECU N° {$currYearLastTwoDigit}/{$pdfDons}"), 1, 1, 'C');

    $pdf->Ln(3);
    $pdf->SetX(228);
    $pdf->SetFont('', '');
    $pdf->SetFontSize(6);
    $pdf->Cell($rightTitleWidth, 4, utf8_decode("MODELE OBLIGATOIRE"), 0, 1, 'C');
    $pdf->SetX(228);
    $pdf->SetFontSize(7);
    $pdf->Cell($rightTitleWidth, 4, utf8_decode("(conformément à l'arrêté du 26.06.2008)"), 0, 1, 'C');
    $pdf->SetFontSize(9);


    $pdf->Ln(5);
    $pdf->Rect(149, 35, 130, 40);
    $pdf->SetX(150);
    $pdf->SetFont('', 'B');
    $pdf->Cell(130, 5, utf8_decode("FONDATION CASIP-COJASOR"), 0, 1, 'C');
    $pdf->SetX(150);
    $pdf->SetFont('', '');
    $pdf->Cell(130, 5, utf8_decode("8, Rue de Pali-Kao, 75020 PARIS - 01 44 62 13 13"), 0, 1, 'C');
    $pdf->SetX(150);
    $pdf->Cell(130, 5, utf8_decode("FONDATION RECONNUE D'UTILITE PUBLIQUE"), 0, 1, 'C');
    $pdf->SetX(150);
    $pdf->Cell(130, 5, utf8_decode("(décret du 29/12/1999 - Journal Officiel du 30/12/1999)"), 0, 1, 'C');

    $pdf->Ln(2);
    $pdf->SetX(152);
    $pdf->Cell(15, 5, utf8_decode("OBJET : "), 0, 0);

    $pdf->SetX(165);
    $pdf->Cell(0, 4, utf8_decode("Service social familial et personnes agées, établissement pour personnes"), 0, 1);
    $pdf->SetX(165);
    $pdf->Cell(0, 4, utf8_decode("âgées, logements sociaux, halte garderie, centre social, foyer-logements"), 0, 1);
    $pdf->SetX(165);
    $pdf->Cell(0, 4, utf8_decode("pour handicapés, fourniture de repas gratuits, services du tutelles."), 0, 1);

    $pdf->Ln(7);
    $pdf->SetX(150);
    $pdf->Cell(130, 4, utf8_decode("Le bénéficiaire certifie sur l'honneur que les dons et versements qu'il reçoit ouvrent "), 0, 1, 'C');
    $pdf->SetX(150);
    $pdf->Cell(130, 4, utf8_decode("droit à réductions d'impôts prévues aux articles :"), 0, 1, 'C');

    $pdf->Ln(2);
    $pdf->SetY(87);
    $pdf->SetX(150);
    $pdf->MultiCell(43, 3, utf8_decode("200 du CGI
Impôt sur le revenu des 
particuliers.
Amendement Coluche au
titre de la fourniture gratuite
de repas
"), 0, 'C');

    $pdf->SetY(87);
    $pdf->SetX(193);
    $pdf->MultiCell(43, 3, utf8_decode("238 bis du CGI
Impôt sur les sociétés
"), 0, 'C');

    $pdf->SetY(87);
    $pdf->SetX(236);
    $pdf->MultiCell(43, 3, utf8_decode("885 - 0 V bis A du CGI
Impôt de solidarité sur la 
fortune (ISF)
"), 0, 'C');

    $pdf->Rect(149, 107, 130, 80);
    $pdf->SetY(108);
    $pdf->SetX(150);
    $pdf->Cell(0, 4, utf8_decode("La Fondation CASIP-COJASOR reconnaît avoir reçu à titre de don la somme de"), 0, 1);

    $pdf->Ln(2);
    $pdf->SetX(152);
    $pdf->SetFontSize(13);
    $moneyWidth = $pdf->GetStringWidth(utf8_decode("** {$montantMembres} Euros **")) + 3;
    $pdf->Cell($moneyWidth, 6, utf8_decode("** {$montantMembres} Euros **"), 1, 0, 'C');

    $pdf->SetFontSize(9);
    $pdf->SetX(150 + $moneyWidth + 3);
    $pdf->Cell(130 - $moneyWidth - 3, 7, utf8_decode("en date du {$dateDons}"), 0, 1, 'C');

    $pdf->SetX(150);
    $pdf->Cell(130, 9, utf8_decode("Cinquante Euros"), 0, 1);


    $pdf->SetFontSize(8);

// Forme checkboxes /////////////////////////////////
    $pdf->SetFillColor(150, 150, 150);
    $pdf->SetLineWidth(0.2);
    $pdf->Rect(165, 130.9, 3, 3);
    $pdf->Rect(195, 130.9, 3, 3);
    $pdf->Rect(234.3, 130.9, 3, 3);
    $pdf->Rect(261.2, 130.9, 3, 3);

    $pdf->SetDrawColor(0);
    $pdf->SetLineWidth(0.4);

    switch ($typeDons) {
        case '1':
            $pdf->Line(165.5, 131.9, 166, 133);
            $pdf->Line(166, 133, 167.5, 131.5);
            break;
        case '2':
            $pdf->Line(195.5, 131.9, 196, 133);
            $pdf->Line(196, 133, 197.5, 131.5);
            break;
        case '3':
            $pdf->Line(234.8, 131.9, 235.3, 133);
            $pdf->Line(235.3, 133, 236.8, 131.5);
            break;
        case '4':
            $pdf->Line(261.7, 131.9, 262.2, 133);
            $pdf->Line(262.2, 133, 263.7, 131.5);
            break;
    }

    $pdf->SetX(150);
    $pdf->Cell(130, 6, utf8_decode("Forme :           Acte authentique           Acte ss seing privé                    Don manuel               Autre"), 0, 1);
// end Forme checkboxes//////////////////////////////

// Nature checkboxes ////////////////////////////////
    $pdf->SetDrawColor(150);
    $pdf->SetLineWidth(0.2);
    $pdf->Rect(165, 137, 3, 3);
    $pdf->Rect(195, 137, 3, 3);
    $pdf->Rect(234.3, 137, 3, 3);

    $pdf->SetDrawColor(0);
    $pdf->SetLineWidth(0.4);

    switch ($natureDons) {
        case '1':
            $pdf->Line(165.5, 138, 166, 139);
            $pdf->Line(166, 139, 167.5, 137.5);
            break;
        case '2':
            $pdf->Line(195.5, 138, 196, 139);
            $pdf->Line(196, 139, 197.5, 137.5);
            break;
        case '3':
            $pdf->Line(234.8, 138, 235.3, 139);
            $pdf->Line(235.3, 139, 236.8, 137.5);
            break;
    }

    $pdf->SetX(150);
    $pdf->Cell(130, 6, utf8_decode("Nature :          Numéraire                      Titres de sociétés cotés            Autre"), 0, 1);

// End Nature checkboxes ////////////////////////////

// Mode checkboxes //////////////////////////////////
    $pdf->SetDrawColor(150);
    $pdf->SetLineWidth(0.2);
    $pdf->Rect(165, 143, 3, 3);
    $pdf->SetFillColor(0, 0, 0);
    $pdf->Rect(195, 143, 3, 3);
    $pdf->SetFillColor(0, 0, 0);
    $pdf->Rect(214, 143, 3, 3);
    $pdf->SetFillColor(0, 0, 0);

    $pdf->SetDrawColor(0);
    $pdf->SetLineWidth(0.4);
    switch ($modeDons) {
        case '1':
            $pdf->Line(165.5, 144, 166, 145);
            $pdf->Line(166, 145, 167.5, 143.5);
            break;
        case '2':
            $pdf->Line(195.5, 144, 196, 145);
            $pdf->Line(196, 145, 197.5, 143.5);
            break;
        case '3':
            $pdf->Line(214.8, 144, 215.3, 145);
            $pdf->Line(215.3, 145, 216.8, 143.5);
            break;
    }

    $pdf->SetX(150);
    $pdf->Cell(130, 6, utf8_decode("Mode :            Remise d'espèces         Chèque            Virement, prélèvement, mandat, carte bancaire"), 0, 1);
// End Mode checkboxes //////////////////////////////

    $pdf->Ln(1);
    $pdf->SetFontSize(11);
    $pdf->SetX(182);
    $pdf->Cell(0, 9, utf8_decode("de :"), 0, 0);

    $pdf->SetY(151);
    $pdf->SetX(182 + $pdf->GetStringWidth(utf8_decode("de :")) + 2);
    $pdf->SetDrawColor(150);
    $pdf->MultiCell(87, 5, utf8_decode($titreCiv . " " . strtoupper($nomMembres) . " " . strtoupper($prenomMembres) . "
{$adresseMembres}
{$cpMembres} " . strtoupper($villeMembres) .
        ""), 1, 'L');

    $pdf->SetFontSize(9);
    $pdf->Ln(9);
    $pdf->SetX(150);
    $pdf->Cell(0, 5, utf8_decode("Paris, le {$dateDons}"), 0, 0);

    $pdf->SetX(205);
    $pdf->Cell(0, 5, utf8_decode("Le Directeur :"), 0, 1);

    $pdf->Output("../pdf/" . $pdfDons . ".pdf", "F");
}

$idDon = isset($_GET['id']) ? $_GET['id'] : false;

if (!$idDon) {
    header('Location: ../index.php');
}

$ex_pdo = new PDOp();
$data = $ex_pdo->getForTemplate($idDon);
$data = $data[0];

makePdf($data);

header('Location: ../pages/pdf.php');

