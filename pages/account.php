<?php
require_once('../lib/lib_msql.php');
require_once('../lib/lib_pdf.php');
session_start();

$nom = isset($_POST['nom']) ? $_POST['nom'] : false;
$nomDeFam = isset($_POST['nomDeFam']) ? $_POST['nomDeFam'] : false;

if(!$nom || !$nomDeFam){
    $_SESSION['nomembres'] = 'nomembres';
    header( 'Location: ../index.php' );
}

$fpath = '../pdf/';
$ext = '.pdf';
$pdficon = 'pdf-icon.png';
$files = scandir($fpath);
$ex_pdo = new PDOp();
$existedMember = $ex_pdo->checkIfExisted($nom,$nomDeFam);
$data = $ex_pdo->getbyName($nom,$nomDeFam);

if(!$existedMember){
    $_SESSION['nomembres'] = 'nomembres';
    header( 'Location: ../index.php' );
}

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Member Account</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <link rel="stylesheet" href="../css/style.css" type="text/css" />
</head>
<body>
    <div class="centre">
        <? if ($data) :?>
        <table class="table shadowBox">
            <tr><th class="tableDate">Date</th><th class="amount">Montant</th><th class="donation">Reçus</th>
			 <?
            foreach($data as $item_data){
                if($item_data['pdf_dons'] != '' && in_array($item_data['pdf_dons'].$ext, $files) && $item_data['valide_dons'] == 1){
                    $picture = '<div class="pdf"><a href="'.$fpath . $item_data['pdf_dons']. $ext.'"  target="_blank"><img src="../images/pdf-icon.png"></a></div>';
                    } elseif($item_data['pdf_dons'] !='' && !in_array($item_data['pdf_dons'].$ext, $files) && $item_data['valide_dons'] == 1){
                        $picture = '<div class="gen"><img src="../images/gen-icon.png"></div>';
                    } elseif ($item_data['valide_dons'] == 0) {
                        $picture = '<div class="pdf"><img src="../images/novalid-icon.png"></div>';
                    } else $picture = 'non pas au nom de base de données';
                echo  '<tr><td>'.$item_data['date_dons'].'</td><td>'.$item_data['montant_membres'].'</td><td class="img-icon">'. $picture . '</td></tr>';
            }
            ?>			
        </table>
        <? else : ?>
        <table class="table shadowBox">
            <tr><th class="tableDate">Date</th><th class="amount">Montant</th><th class="donation">Reçus</th>
                <? echo  '<tr><td></td><td></td><td class="img-icon"></td></tr>'; ?>
        </table>
        <? endif ?>
                <br>
        <a class="cyan" href="../index.php">Retour aux dons</a>
    </div>
</body>
</html>