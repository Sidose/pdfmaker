<?php
require_once('../lib/lib_msql.php');

$fpath = '../pdf/';
$ext = '.pdf';
$files = scandir($fpath);

$ex_pdo = new PDOp();
$data = $ex_pdo->getAll();

?>
<!DOCTYPE HTML>
<html>
<head>
    <title>Member Account</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link rel="stylesheet" href="../css/style.css" type="text/css" />
</head>
    <body>
        <div class="centre">

        <table class="table shadowBox">
            <tr><th class="tableDate">Date</th><th class="member">Membre</th><th class="member">Montant</th><th class="donation">Reçus</th><th class="member">Id erreur</th>
            <?
            foreach($data as $item_data){
                $id = $item_data['id_dons'];
                if($item_data['pdf_dons'] != '' && in_array($item_data['pdf_dons'].$ext, $files) && $item_data['valide_dons'] == 1){
                    $picture = '<div class="pdf"><a href="'.$fpath . $item_data['pdf_dons']. $ext.'"  target="_blank"><img src="../images/pdf-icon.png"></a></div>';
                    } elseif($item_data['pdf_dons'] != '' && !in_array($item_data['pdf_dons'].$ext, $files) && $item_data['valide_dons'] == 1){
                        $picture = '<a href="../lib/template.php?id='.$id.'"><div class="gen"><img src="../images/gen-icon.png"></div></a>';
                    } elseif ($item_data['valide_dons'] == 0) {
                        $picture = '<div class="pdf"><img src="../images/novalid-icon.png"></div>';
                    } else $picture = 'non pas au nom de base de données';
                echo  '<tr><td>'.$item_data['date_dons'].'</td><td>'.$item_data['nom_membres'].' '.$item_data['prenom_membres'].'</td><td>'.$item_data['montant_membres'].'€</td><td class="img-icon">'. $picture . '</td><td class="img-icon">'.$item_data['id_erreur'].'</td></tr>';
            }
            ?>

            <table>
        <br>
            <a class="cyan" href="../index.php">Retour aux dons principales</a>
        </div>
    </body>
</html>